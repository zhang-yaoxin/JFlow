package bp.port;
/** 
 操作员属性
*/
public class EmpAttr extends bp.en.EntityNoNameAttr
{

		///基本属性
	 public static final String UserID = "FK_Dept";
	public static final String OrgNo = "FK_Dept";
	/** 
	 部门
	*/
	public static final String FK_Dept = "FK_Dept";
	/** 
	 密码
	*/
	public static final String Pass = "Pass";
	/** 
	 sid
	*/
	public static final String SID = "SID";
	/** 
	 手机号
	*/
	public static final String Tel = "Tel";
	 /** 
	 邮箱
	 */
	public static final String Email = "Email";
	/**
	 * 用户状态 0注销 1在职
	 */
	public static final String UserType="UserType";

		///
}