package bp.wf.template;
import bp.en.EntityTreeAttr;

/** 
 流程类别属性
*/
public class FlowSortAttr extends EntityTreeAttr
{
	/** 
	 组织编号
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 域/系统编号
	*/
	public static final String Domain = "Domain";
}